CREATE DATABASE  IF NOT EXISTS `team4-employee-information-bi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `team4-employee-information-bi`;

DROP TABLE IF EXISTS `reporting`;

CREATE TABLE `reporting` (
  `uuid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `leaves_ratio` double NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

